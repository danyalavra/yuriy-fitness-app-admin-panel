const api = "https://vkrnowversion.firebaseio.com/";

const Entities = {
  productsTypes: api + "productTypes.json",
  users: api + "users.json",
  user: api + "users/",
};

const commonRequest = (
  url = "",
  method = "GET",
  success = () => {},
  data = {}
) => {
  var settings = {
    url,
    method,
    data: JSON.stringify(data),
  };

  $.ajax(settings).done(success);
};

const ProductTypes = {
  getAll: (success) => commonRequest(Entities.productsTypes, "GET", success),
};

const Users = {
  getAll: (success) => commonRequest(Entities.users, "GET", success),
  delete: (data = { id: "" }, success) =>
    commonRequest(Entities.user + data.id + ".json", "DELETE", success),
  add: (
    data = {
      login: "",
      password: "",
      name: "",
      sex: "",
      weight: "",
    },
    success
  ) => commonRequest(Entities.users, "POST", success, data),
  update: (
    id,
    data = {
      login: "",
      password: "",
      name: "",
      sex: "",
      weight: "",
    },
    success
  ) => commonRequest(Entities.user + id + ".json", "PUT", success, data),
};

$(function () {
  // Custom JS
});
