<h1><strong>Фитнес-приложение</strong> <br>Административная панель</h1>

<strong>0_1. Скачать LTS версию NodeJS https://nodejs.org/en/</strong><br>
<strong>0_2. Установить git https://git-scm.com/downloads</strong><br>

<br>
Следующий список команд выполняем в консоли, находясь в директории, где хотим разместить проект
<br>
<strong>folder_name</strong> – название папки, где будет храниться проект
<br>
<strong>1. git clone https://gitlab.com/danyalavra/yuriy-fitness-app-admin-panel.git folder_name</strong><br>
<strong>2. cd folder_name</strong><br>
<strong>3. npm i</strong><br>
<strong>4. npm i -g gulp-cli</strong><br>
<strong>5. npm i --save-dev gulp</strong><br>
<strong>6. gulp</strong>
